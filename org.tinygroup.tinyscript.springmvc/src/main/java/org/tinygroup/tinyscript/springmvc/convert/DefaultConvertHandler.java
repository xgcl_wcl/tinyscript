package org.tinygroup.tinyscript.springmvc.convert;

import org.springframework.http.MediaType;
import org.tinygroup.tinyscript.mvc.StringConvertHandler;

/**
 * 默认转换String
 * @author yancheng11334
 *
 */
public class DefaultConvertHandler extends StringConvertHandler<MediaType>{

	public boolean isMatch(Object obj, MediaType accept) {
		return true;
	}

	protected String convert(Object obj, MediaType accept) {
		return obj==null?null:obj.toString();
	}

	protected String getContentType() {
		return MediaType.TEXT_HTML_VALUE;
	}

	
}
