package org.tinygroup.tinyscript.expression.convert;

import java.math.BigDecimal;

import org.tinygroup.tinyscript.expression.Converter;

public class BigDecimalLong implements Converter<BigDecimal,Long> {

	public Long convert(BigDecimal object) {
		return object.longValue();
	}

	public Class<?> getSourceType() {
		return BigDecimal.class;
	}

	public Class<?> getDestType() {
		return Long.class;
	}

}
